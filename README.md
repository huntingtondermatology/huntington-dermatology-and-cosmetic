Dr. Phyllis J. Smith is a dermatologist in Huntington, NY. She is certified by the American Board of Dermatology and has over 20 years experience in diseases and surgery of the skin. Dr. Smith completed her medical school training at Nassau University Medical Center, affiliated with Health Sciences Center of New York at Stony Brook. She is one of only a few physicians that has completed dual residencies and obtained dual board certifications in both Pediatrics and Dermatology. She completed intense clinical training, completing her internship and residency in Pediatrics at Brookdale Hospital, in Brooklyn New York, and her dermatology residency in SUNY Downstate Medical Center in Brooklyn.

Address: 200 W Carver Street, Suite 2, Huntington, NY 11743

Phone: 631-424-3376